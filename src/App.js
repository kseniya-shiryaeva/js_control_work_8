import './App.css';
import {Route, Switch} from "react-router-dom";
import Header from "./components/Header/Header";
import HomePage from "./containers/HomePage/HomePage";
import AddPage from "./containers/AddPage/AddPage";
import EditPage from "./containers/EditPage/EditPage";

function App() {
  return (
    <div className="App">
      <Header />
      <div className="content">
        <Switch>
          <Route path="/" exact component={HomePage} />
          <Route path="/quotes/:category" exact component={HomePage} />
          <Route path="/add-quote" component={AddPage} />
          <Route path="/quotes/:id/edit" component={EditPage} />
          <Route render={()=><h1>404: Page is not found</h1>} />
        </Switch>
      </div>
    </div>
  );
}

export default App;
