import React from 'react';
import {NavLink} from "react-router-dom";
import './Header.css';
import SiteMenu from "../SiteMenu/SiteMenu";

const Header = () => {
    return (
        <div className="Header">
            <NavLink to="/"><span className="logo">Quotes Glossary</span></NavLink>
            <SiteMenu />
        </div>
    );
};

export default Header;