import React from 'react';
import Categories from "../../config/categories";
import {NavLink} from "react-router-dom";

const SidebarMenu = () => {
    return (
        <div className="SidebarMenu">
            <h3>Categories</h3>
            <ul>
                <li><NavLink to="/">All</NavLink></li>
                {Categories.map(category => {
                    return <li key={category.id}><NavLink to={"/quotes/" + category.id}>{category.title}</NavLink></li>
                })}
            </ul>
        </div>
    );
};

export default SidebarMenu;